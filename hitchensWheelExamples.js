var examples = {};

examples["CHRISTOPHERHITCHENS"] = "4,Christopher Hitchens - \"When will God appear?\",quotation,0\n\
6,First Civilisation{newline}10{comma}000BC,textWithLine,36\n\
8,Attacked by wilderbeasts,textWithLine,72\n\
9,First tablets produced{newline}2{comma}270BC,textWithLine,90\n\
10,Tribal Warfare,textWithLine,108\n\
11,Tribe wiped out,textWithLine,126\n\
13,Tribal Warfare,textWithLine,162\n\
15,Disease,textWithLine,198\n\
15,Epicurus.jpg,image,198\n\
17,Flood,textWithLine,234\n\
20,Earthquakes,textWithLine,288\n\
22,Famine,textWithLine,0\n\
24,God Appears 1000BC,quotation,338\n\
25,God Appears{newline}1000BC,textWithLine,356\n\
28,Jesus Appears,textWithLine,344\n\
30,,quotation,358";

examples["WARS"] = "1,A list of some Wars...,quotation,0\n\
1,Wellington_at_Waterloo_Hillingford.jpg,image,0\n\
1,windowSize,parameter,90\n\
1,1803-1815{newline} Napoleonic Wars,textWithLine,0\n\
3,1804-1813{newline} Russo-Persian War,textWithLine,0\n\
4,1808-1810{newline} Rum Rebellion ,textWithLine,0\n\
5,1808-1833{newline} Spanish American wars of independence ,textWithLine,0\n\
6,1810-1821{newline} Mexican War of Independence,textWithLine,0\n\
7,1812-1815{newline} War of 1812 ,textWithLine,0\n\
8,1813-1814{newline} Creek War ,textWithLine,0\n\
9,1817-1858{newline} Seminole Wars,textWithLine,0\n\
10,1818-1828{newline} Zulu Wars of Conquest ,textWithLine,0\n\
11,1820-1875{newline} Texasâ€“Indian wars ,textWithLine,0\n\
12,1821-1832{newline} Greek War of Independence ,textWithLine,0\n\
14,1821-1848{newline} Comancheâ€“Mexico War ,textWithLine,0\n\
16,1825-1830{newline} Java War ,textWithLine,0\n\
18,1827-1827{newline} Winnebago War ,textWithLine,0\n\
20,1832-1832{newline} Black Hawk War ,textWithLine,0\n\
22,1835-1836{newline} Texas Revolution ,textWithLine,0\n\
24,1839-1842{newline} First Opium War ,textWithLine,0\n\
26,1846-1864{newline} Navajo Wars ,textWithLine,0\n\
28,1846-1848{newline} Mexican-American War ,textWithLine,0\n\
28,vietnam-war-01.jpg,image,0\n\
30,1849-1924{newline} Apache Wars ,textWithLine,0\n\
32,1850-1865{newline} California Indian Wars ,textWithLine,0\n\
34,1853-1856{newline} Crimean War ,textWithLine,0\n\
36,1861-1865{newline} American Civil War ,textWithLine,0\n\
38,1864-1868{newline} Snake War,textWithLine,0\n\
40,1866-1868{newline} Red Cloud's War ,textWithLine,0\n\
44,1867-1875{newline} Comanche Campaign ,textWithLine,0\n\
47,1876-1877{newline} Great Sioux War (Black Hills War) ,textWithLine,0\n\
49,1877-1877{newline} Nez Perce War ,textWithLine,0\n\
51,1878-1879{newline} Cheyenne War ,textWithLine,0\n\
52,1879-1879{newline} Sheepeater Indian War ,textWithLine,0\n\
54,1879-1880{newline} Victorio's War ,textWithLine,0\n\
70,1899-1901{newline} Boxer Rebellion ,textWithLine,0\n\
100,1899-1902{newline} Second Boer War,textWithLine,0";

examples["HELL"] = "1,When does hell appear?,quotation,0\n\
2,bible.jpg,image,0\n\
2,Authorized King James Version (KJV) based on corrupted texts 31 23 54,textWithLine,0\n\
3,New King James Version (NKJV) still wrong about Sheol 19 13 32,textWithLine,0\n\
5,New International Version (NIV) the best-selling English Bible 0 13 13,textWithLine,0\n\
7,American Standard Version (ASV) 0 13 13,textWithLine,0\n\
9,New American Standard Bible (NASB) 0 13 13,textWithLine,0\n\
11,Holman Christian Standard Bible (HCSB) Southern Baptist 0 11 11,textWithLine,0\n\
13,Revised Standard Version (RSV) 0 12 12,textWithLine,0\n\
15,New Revised Standard Version (NRSV) 0 12 12,textWithLine,0\n\
17,Revised English Bible (REB) 0 13 13,textWithLine,0\n\
19,New Living Translation (NLT) 0 13 13,textWithLine,0\n\
21,Amplified Bible (AMP) 0 13 13,textWithLine,0\n\
23,Darby 0 12 12,textWithLine,0\n\
25,New Century Version (NCV) 0 12 12,textWithLine,0\n\
26,New American Bible Revised Edition (NABRE) Roman Catholic 0 0 0,textWithLine,0\n\
28,Wesley's New Testament (1755) 0	0 0 ,textWithLine,0\n\
30,Scarlett's N.T. (1798) 0	0 0 ,textWithLine,0\n\
32,The New Testament in Greek and English (Kneeland 1823) 0	0 0 ,textWithLine,0\n\
34,Young's Literal Translation (1891) 0 0 0,textWithLine,0\n\
36,Twentieth Century New Testament (1900) 0	0 0 ,textWithLine,0\n\
38,Rotherham's Emphasized Bible (reprinted 1902) 0 0 0,textWithLine,0\n\
40,Fenton's Holy Bible in Modern English (1903) 0 0 0,textWithLine,0\n\
42,Weymouth's New Testament in Modern Speech (1903) 0	0 0 ,textWithLine,0\n\
42,http://media.npr.org/assets/artslife/arts/2010/02/dantesinferno/inferno_archive-becf91cdf5140a602f77b5514b518ba7db4db4f6-s900-c85.jpg,image,0\n\
44,Jewish Publication Society Bible Old Testament (1917) 0 0	0 ,textWithLine,0\n\
46,Panin's Numeric English New Testament (1914) 0	0 0 ,textWithLine,0\n\
48,The People's New Covenant (Overbury 1925) 0	0 0 ,textWithLine,0\n\
50,Hanson's New Covenant (1884) 0	0 0 ,textWithLine,0\n\
52,Western N.T. (1926) 0	0 0 ,textWithLine,0\n\
54,NT of our Lord and Savior Anointed (Tomanek 1958) 0	0 0 ,textWithLine,0\n\
56,Concordant Literal NT (1983) 0	0 0 ,textWithLine,0\n\
58,The N.T., A Translation (Clementson 1938) 0	0 0 ,textWithLine,0\n\
60,Emphatic Diaglott Greek/English Interlinear (Wilson 1942) 0	0 0 ,textWithLine,0\n\
62,New American Bible (1970) 0 0 0,textWithLine,0\n\
64,Restoration of Original Sacred Name Bible (1976) 0 0 0,textWithLine,0\n\
66,Tanakh- The Holy Scriptures; Old Testament (1985) 0 0	0 ,textWithLine,0\n\
68,The New Testament; A New Translation (Greber  1980) 0	0 0 ,textWithLine,0\n\
70,Christian Bible (1991) 0 0 0,textWithLine,0\n\
72,World English Bible (in progress) 0 0 0,textWithLine,0\n\
74,Orthodox Jewish Brit Chadasha [NT Only] 0	0 0 ,textWithLine,0\n\
76,Original Bible Project (Dr. James Tabor; still in translation) 0 0 0,textWithLine,0\n\
78,Zondervan Parallel N.T. in Greek and English (1975)** 0	0 0 ,textWithLine,0\n\
80,Int. NASB-NIV Parallel N.T. in Greek and English (1993)** 0	0 0 ,textWithLine,0\n\
82,A Critical Paraphrase of the N.T. by Vincent T. Roth (1960) 0	0 0,textWithLine,0\n\
82,https://victorianeuronotes.files.wordpress.com/2015/08/heaven-hell-christian-sociopaths.jpg?w=820&h=597,image,0";
